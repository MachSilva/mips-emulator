CXX			= g++
override CXXFLAGS	+= -Wall -std=c++11 -O3 -march=native -Wno-packed-bitfield-compat
MARS			= Mars4_5.jar

SRCFILES		= main.cpp $(addprefix mips/, mips.cpp syscall.cpp disassembler.cpp)

OUTPUT			= bin/mips-emulator

.PHONY: all
all: $(addprefix src/, $(SRCFILES))
	# Building $(OUTPUT)
	@$(CXX) $^ -o $(OUTPUT) $(CXXFLAGS)

.PHONY: clean
clean:
	# Cleaning binaries and solution files
	@rm -f bin/* data/*.sol

.PHONY: test
test:
	# To be implemented

$(MARS):
	# MARS not found!
	# Please, download MARS and place it into $(MARS)
	@exit -1

data/%.in:
	# Warning!
	# No test input for program found!
	# Solution generation may be wrong, or get in a LOOP
	@touch $@

%: data/%.asm $(MARS) data/%.in
	# Assembling $<
	@java -jar $(MARS) nc mc CompactTextAtZero dump .text Binary bin/$@.text dump .data Binary bin/$@.data a $<
	# Generating SOLUTION file from MARS
	@java -jar $(MARS) nc mc CompactTextAtZero \$$0 \$$1 \$$2 \$$3 \$$4 \$$5 \$$6 \$$7 \$$8 \$$9 \$$10 \$$11 \$$12 \$$13 \$$14 \$$15 \$$16 \$$17 \$$18 \$$19 \$$20 \$$21 \$$22 \$$23 \$$24 \$$25 \$$26 \$$27 \$$28 \$$29 \$$30 \$$31 0x0-0x3ffc $< < data/$@.in > data/$@.sol 2>&1
