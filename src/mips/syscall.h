/**
 * @file    syscall.h
 * @author  Felipe Silva
 * @brief   Contains SYSCODE definition and standard syscall handler.
 * @version 0.1
 * @date    2018-10-12
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#pragma once

namespace mips {
    /**
     * @brief Syscall handler return code.
     * 
     * This tells to execution routine the syscall status.
     * It can be ok or a exit request.
     * 
     */
    enum SYSCODE {
        OK = 0, EXIT_REQUEST
    };

    /* There is a class!
     * If we include "mips.h", we get a recursive dependency between
     * "mips.h" and "syscall.h" */
    class MIPS;

    /**
     * @brief Standard syscall handler for the MIPS.
     * 
     * Compatible with MARS on a limited service number:
     * services 1, 4, 5, 8, 10, 11, 12, 34, 35 and 36.
     * See http://courses.missouristate.edu/kenvollmar/mars/Help/SyscallHelp.html.
     * 
     * @param   MIPS*   An instance of MIPS virtual machine where service was requested.
     * @return  SYSCODE A possible request to MIPS execution routine.
     */
    SYSCODE std_syscall_handler(MIPS*);
}
