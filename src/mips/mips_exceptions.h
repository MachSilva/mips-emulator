/**
 * @file    mips_exceptions.h
 * @author  Felipe Silva
 * @brief   Here we define a set of MIPS exception that may occur.
 * @version 0.1
 * @date    2018-10-09
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#pragma once

#include <stdexcept>

namespace mips {
    class mips_exception : public std::exception
    {
    public:
        virtual const char * what() const throw ()
        {
            return "MIPS CPU unknown exception\n";
        }
    };

    class stack_overflow : public mips_exception
    {
    public:
        virtual const char * what() const throw ()
        {
            return "Error: stack overflow.\n";
        }
    };

    class invalid_access : public mips_exception
    {
    public:
        virtual const char * what() const throw ()
        {
            return "Invalid memory access!\n";
        }
    };

    class divide_by_zero : public mips_exception
    {
    public:
        virtual const char * what() const throw ()
        {
            return "Divide by zero.\n";
        }
    };
}
