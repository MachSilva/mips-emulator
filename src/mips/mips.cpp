#include "mips.h"

#include <cassert>
#include <cstring>
#include <iomanip>
#include <ostream>
#include "syscall.h"

using namespace mips;

MIPS::MIPS(uint32_t ram_size)
: ram_size(ram_size), ram(new int8_t[ram_size]), syscall(std_syscall_handler)
{
    clear();
}

MIPS::~MIPS()
{
    delete[] ram;
}

void MIPS::clear()
{
    memset(ram, 0, ram_size);
    for (auto& r : reg)
        r = 0;
    hilo = 0;
    pc = 0;
    instruction_counter = 0;
}

/* MIPS processor execute routine.
 * Note that here instructions are processed with
 * a switch block despite using classes. This, along
 * with inline code (avoiding function calls),
 * enables compiler's optimization features, like jump tables
 * and binary search. So, make sure to compile the project
 * with at least "-O1" flag. */
void MIPS::execute()
{
    while (true)
    {
        // Check stack
        if (reg[MIPS_SP] < 0x3000)
            throw stack_overflow();
        
        // Check $pc for .text section end
        if (pc >= 0x0FFF)
            return;

        // Fetch instruction
        auto ins = load<InstructionWord>(pc);
        uint32_t addr;

        // Hard wired register
        reg[MIPS_ZERO] = 0;

        if (monitor)
            monitor(this);
        
        // Increment pc
        pc += 4;
        instruction_counter++;

        // Process opcode
        switch (ins.R.opcode)
        {
        // Type R
        case 0x00:
            switch (ins.R.funct)
            {
            // SLL
            case 0x00:
                reg[ins.R.rd] = reg[ins.R.rt] << ins.R.shamt;
                break;
            // SRL
            case 0x02:
                reg[ins.R.rd] = reg[ins.R.rt] >> ins.R.shamt;
                break;
            // SRA
            case 0x03:
                reg[ins.R.rd] = ((int32_t) reg[ins.R.rt]) >> ins.R.shamt;
                break;
            // SLLV
            case 0x04:
                reg[ins.R.rd] = reg[ins.R.rt] << reg[ins.R.rs];
                break;
            // SRLV
            case 0x06:
                reg[ins.R.rd] = reg[ins.R.rt] >> reg[ins.R.rs];
                break;
            // SRAV
            case 0x07:
                reg[ins.R.rd] = ((int32_t) reg[ins.R.rt]) >> reg[ins.R.rs];
                break;
            // JR
            case 0x08:
                pc = reg[ins.R.rs];
                break;
            // JALR
            case 0x09:
                reg[ins.R.rd] = pc;
                pc = reg[ins.R.rs];
                break;
            // SYSCALL
            case 0x0C:
                if (syscall(this) == SYSCODE::EXIT_REQUEST)
                    return;
                break;
            // BREAK
            case 0x0D:
                return;
            // MFHI
            case 0x10:
                reg[ins.R.rd] = hi;
                break;
            // MTHI
            case 0x11:
                hi = reg[ins.R.rs];
                break;
            // MFLO
            case 0x12:
                reg[ins.R.rd] = lo;
                break;
            // MTLO
            case 0x13:
                lo = reg[ins.R.rs];
                break;
            // MULT
            case 0x18:
                hilo = ((int64_t) reg[ins.R.rs]) * (int64_t) reg[ins.R.rt];
                break;
            // MULTU
            case 0x19:
                hilo = reg[ins.R.rs] * reg[ins.R.rt];
                break;
            // DIV
            case 0x1A:
                if (reg[ins.R.rt] == 0)
                    throw divide_by_zero();
                    
                lo = ((int32_t) reg[ins.R.rs]) / (int32_t) reg[ins.R.rt];
                hi = ((int32_t) reg[ins.R.rs]) % (int32_t) reg[ins.R.rt];
                break;
            // DIVU
            case 0x1B:
                if (reg[ins.R.rt] == 0)
                    throw divide_by_zero();

                lo = reg[ins.R.rs] / reg[ins.R.rt];
                hi = reg[ins.R.rs] % reg[ins.R.rt];
                break;
            // ADD
            case 0x20:
                reg[ins.R.rd] = ((int32_t) reg[ins.R.rs]) + (int32_t) reg[ins.R.rt];
                break;
            // ADDU
            case 0x21:
                reg[ins.R.rd] = reg[ins.R.rs] + reg[ins.R.rt];
                break;
            // SUB
            case 0x22:
                reg[ins.R.rd] = ((int32_t) reg[ins.R.rs]) - (int32_t) reg[ins.R.rt];
                break;
            // SUBU
            case 0x23:
                reg[ins.R.rd] = reg[ins.R.rs] - reg[ins.R.rt];
                break;
            // AND
            case 0x24:
                reg[ins.R.rd] = reg[ins.R.rs] & reg[ins.R.rt];
                break;
            // OR
            case 0x25:
                reg[ins.R.rd] = reg[ins.R.rs] | reg[ins.R.rt];
                break;
            // XOR
            case 0x26:
                reg[ins.R.rd] = reg[ins.R.rs] ^ reg[ins.R.rt];
                break;
            // NOR
            case 0x27:
                reg[ins.R.rd] = ~(reg[ins.R.rs] | reg[ins.R.rt]);
                break;
            // SLT
            case 0x2A:
                reg[ins.R.rd] = ((int32_t) reg[ins.R.rs]) < (int32_t) reg[ins.R.rt];
                break;
            // SLTU
            case 0x2B:
                reg[ins.R.rd] = reg[ins.R.rs] < reg[ins.R.rt];
                break;
            }
            break;
        // Branch
        case 0x01:
            switch (ins.I.rt)
            {
            // BLTZ
            case 0x00:
                if (((int32_t) reg[ins.I.rs]) < 0)
                    pc += (int16_t)(ins.I.immediate << 2);
                break;
            // BGEZ
            case 0x01:
                if (((int32_t) reg[ins.I.rs]) >= 0)
                    pc += (int16_t)(ins.I.immediate << 2);
                break;
            }
            break;
        // JAL
        case 0x03:
            reg[MIPS_RA] = pc;
        // J
        case 0x02:
            pc = (pc & 0xF0000000) | (ins.J.immediate << 2);
            break;
        // BEQ
        case 0x04:
            if (reg[ins.I.rs] == reg[ins.I.rt])
                pc += (int16_t)(ins.I.immediate << 2);
            break;
        // BNE
        case 0x05:
            if (reg[ins.I.rs] != reg[ins.I.rt])
                pc += (int16_t)(ins.I.immediate << 2);
            break;
        // BLEZ
        case 0x06:
            if (((int32_t) reg[ins.I.rs]) <= 0)
                pc += (int16_t)(ins.I.immediate << 2);
            break;
        // BGTZ
        case 0x07:
            if (((int32_t) reg[ins.I.rs]) > 0)
                pc += (int16_t)(ins.I.immediate << 2);
            break;
        // ADDI
        case 0x08:
            reg[ins.I.rt] = ((int32_t) reg[ins.I.rs]) + (int32_t) ins.I.immediate;
            break;
        // ADDIU
        case 0x09:
            reg[ins.I.rt] = reg[ins.I.rs] + ins.I.immediate;
            break;
        // SLTI
        case 0x0A:
            reg[ins.I.rt] = ((int32_t) reg[ins.I.rs]) < ((int32_t) ins.I.immediate);
            break;
        // SLTIU
        case 0x0B:
            reg[ins.I.rt] = ((uint32_t) reg[ins.I.rs]) < ((uint32_t) ins.I.immediate);
            break;
        // ANDI
        case 0x0C:
            reg[ins.I.rt] = reg[ins.I.rs] & ins.I.immediate;
            break;
        // ORI
        case 0x0D:
            reg[ins.I.rt] = reg[ins.I.rs] | ins.I.immediate;
            break;
        // XORI
        case 0x0E:
            reg[ins.I.rt] = reg[ins.I.rs] ^ ins.I.immediate;
            break;
        // LUI
        case 0x0F:
            reg[ins.I.rt] &= 0x0000FFFF;
            reg[ins.I.rt] |= (ins.I.immediate << 16);
            break;
        // LB
        case 0x20:
            addr = reg[ins.I.rs] + (int32_t) ins.I.immediate;
            reg[ins.I.rt] = load<int8_t>(addr);
            break;
        // LH
        case 0x21:
            addr = reg[ins.I.rs] + (int32_t) ins.I.immediate;
            reg[ins.I.rt] = load<int16_t>(addr);
            break;
        // LW
        case 0x23:
            addr = reg[ins.I.rs] + (int32_t) ins.I.immediate;

            reg[ins.I.rt] = load<int32_t>(addr);
            break;
        // SB
        case 0x28:
            addr = reg[ins.I.rs] + (int16_t) ins.I.immediate;
            store(addr, (int8_t) reg[ins.I.rt]);
            break;
        // SH
        case 0x29:
            addr = reg[ins.I.rs] + (int16_t) ins.I.immediate;
            store(addr, (int16_t) reg[ins.I.rt]);
            break;
        // SW
        case 0x2B:
            addr = reg[ins.I.rs] + (int16_t) ins.I.immediate;
            store(addr, reg[ins.I.rt]);
            break;
        }
    }
}

void MIPS::print_registers(std::ostream& out)
{
    char info[32];
    for (int i = 0; i < 32; i++)
    {
        std::snprintf(info, sizeof (info), "$%d\t0x%08x\n", i, reg[i]);
        out << info;
    }
}

void MIPS::print_memory(std::ostream& out)
{
    char info[128];
    for (unsigned int i = 0; i < ram_size; i += 16)
    {
        auto word = load_real_ptr<uint32_t>(i);
        std::snprintf(info, sizeof (info), "Mem[0x%08x]\t0x%08x\t0x%08x\t0x%08x\t0x%08x\t\n",
            i, word[0], word[1], word[2], word[3]);
        out << info;
    }
}
