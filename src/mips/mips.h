/**
 * @file    mips.h
 * @author  Felipe Silva
 * @brief   MIPS main classes, along with template
 *          utilitary methods and InstructionWord definition.
 * @version 0.1
 * @date    2018-10-09
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#pragma once

#include <cstdint>
#include <functional>
#include "mips_exceptions.h"
#include "syscall.h"

#define MIPS_DEFAULT_RAM_SIZE (4096 * 4)
#define MIPS_TEXT 0x0000
#define MIPS_DATA 0x2000
#define MIPS_SECT_MAX 4096

#define MIPS_ZERO 0
#define MIPS_V0 2
#define MIPS_V1 3
#define MIPS_A0 4
#define MIPS_A1 5
#define MIPS_A2 6
#define MIPS_A3 7
#define MIPS_GP 28
#define MIPS_SP 29
#define MIPS_FP 30
#define MIPS_RA 31

namespace mips {
    /* MIPS instruction types */
    union InstructionWord {
        /* Type R */
        struct __attribute__((__packed__)) {
            uint8_t funct:6, shamt:5, rd:5, rt:5, rs:5, opcode:6;
        } R;
        /* Type I */
        struct __attribute__((__packed__)) {
            int16_t immediate;
            uint8_t rt:5, rs:5, opcode:6;
        } I;
        /* Type J */
        struct __attribute__((__packed__)) {
            int32_t immediate:26;
            uint8_t opcode:6;
        } J;
    };

    static_assert(
        sizeof(InstructionWord) == 4,
        "All MIPS instructions structures must be packed into 32 bits!\n"
    );

    /* HalfWord and Word types */
    class HalfWord {
    public:
        union {
            int16_t r16;
            struct {
                int8_t r8, r8b; // LO, HI
            };
        };

        HalfWord() : r16(0) {}
        HalfWord(int8_t v) : r8(v) {}
        HalfWord(int16_t v) : r16(v) {}

        inline void operator = (int8_t rhs) { r8 = rhs; }
        inline void operator = (int16_t rhs) { r16 = rhs; }
    };

    class Word {
    public:
        union {
            int32_t r32;
            struct {
                int16_t r16, r16b;
            };
            struct {
                int8_t r8, r8b, r8c, r8d;
            };
        };

        Word() : r32(0) {}
        Word(int8_t v) : r8(v) {}
        Word(int16_t v) : r16(v) {}
        Word(int32_t v) : r32(v) {}

        inline void operator = (int8_t rhs) { r8 = rhs; }
        inline void operator = (int16_t rhs) { r16 = rhs; }
        inline void operator = (int32_t rhs) { r32 = rhs; }
    };

    static_assert(
        sizeof (HalfWord) == 2 && sizeof (Word) == 4,
        "HalfWord isn't 16 bits or Word isn't 32 bits! Check source code!\n"
    );

    /**
     * @brief MIPS machine structure.
     * 
     * Holds data about the machine, like memory and registers,
     * and is capable to simulate instructions.
     */
    class MIPS {
    public:
        const uint32_t ram_size;    /**< Size of allocated virtual RAM */
        int8_t * ram;               /**< Pointer to a byte array of size ram_size */
        int32_t reg[32];            /**< MIPS register vector */
        uint32_t pc;                /**< Program counter */
        /**
         * @brief HILO register defined here
         * 
         * As a UNION, we may refer to this register as a 64 bits value HILO,
         * as most significant 32 bits HI, or as least significant 32 bits LO.
         * Your choice!
         */
        union {
            int64_t hilo;
            struct {
                int32_t lo, hi;
            };
        };
        uint32_t instruction_counter;           /**< An counter for how many instructions were executed */

        std::function<void(MIPS*)> monitor;     /**< Function to be called before each instruction */
        std::function<SYSCODE(MIPS*)> syscall;  /**< Should return a code (see "syscall.h") */

        /**
         * @brief Construct MIPS machine.
         * 
         * @param   ram_size    Amount of RAM
         */
        MIPS(uint32_t ram_size);
        ~MIPS();

        /**
         * @brief Clears all data.
         * 
         * Fills RAM with zeroes, and clears all registers.
         */
        void clear();

        /**
         * @brief Executes all instructions from starting from PC value.
         * 
         * May throw exceptions. See "mips_exceptions".
         */
        void execute();

        /**
         * @brief Prints all registers into stream.
         * 
         * @param out Output stream where registers will be written.
         */
        void print_registers(std::ostream& out);

        /**
         * @brief Prints all memory into stream.
         * 
         * @param out Output stream where memory will be written.
         */
        void print_memory(std::ostream& out);
        
        /* Auxiliary inline functions: must be in header to compile */
        
        /**
         * @brief Retrieve a genuine pointer to data T at RAM.
         * 
         * Given a MIPS address to simulated RAM,
         * this method returns a real address to its data
         * that can be accessed for any code that doesn't know
         * about this emulation (like printf).
         * 
         * This pointer can be used in our program core.
         * The method may throw invalid_access exception.
         * 
         * @tparam T        Type T we want to read.
         * @param address   MIPS address, or RAM array's index.
         * @return T*       A genuine pointer to data of type T.
         */
        template<typename T>
        inline T* load_real_ptr(uint32_t address)
        {
            if (address >= ram_size)
                throw invalid_access();
            
            return (T*)(ram+address);
        }

        /**
         * @brief Retrive data from RAM and returns as type T&.
         * 
         * Given a MIPS address to simulated RAM,
         * this method returns a reference to type T
         * at the location.
         * The method may throw invalid_access exception.
         * 
         * @tparam T        Type T we want to read.
         * @param address   MIPS address, or RAM array's index.
         * @return T&       A reference to data of type T.
         */
        template<typename T>
        inline T& load(uint32_t address)
        {
            return *load_real_ptr<T>(address);
        }

        /**
         * @brief Write data of type T on MIPS RAM.
         * 
         * Take an address of simulated RAM and write any data 
         * of type T on it.
         * 
         * The method may throw invalid_access exception.
         * 
         * @tparam T        Type/class of data.
         * @param address   MIPS address, or RAM array's index.
         * @param data      Data that will be written.
         */
        template<typename T>
        inline void store(uint32_t address, T data)
        {
            load<T>(address) = data;
        }
    };
}
