/**
 * @file    disassembler.h
 * @author  Felipe Silva
 * @brief   Simple disassembler for live code watching
 * @version 0.1
 * @date    2018-10-14
 * 
 * @copyright Copyright (c) 2018
 * 
 */
#pragma once

#include "mips.h"
#include <string>

namespace mips {
    /**
     * @brief Get the mnemonic string
     * 
     * @param i A valid MIPS Instruction Word
     * @return const char* String with mnemonic
     */
    const char * get_mnemonic(InstructionWord i);

    /**
     * @brief Get the operands of instruction as string
     * 
     * @param i A valid MIPS Instruction Word
     * @return std::string A string with instruction's operands.
     */
    std::string get_operands(InstructionWord i);
}
