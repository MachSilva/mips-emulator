#include "disassembler.h"

#define LEN(v) (sizeof (v) / sizeof (v[0]))

static const char * opcodeAsZero[] = {
    "sll",      // 0x00
    0,
    "srl",      // 0x02
    "sra",      // 0x03
    "sllv",     // 0x04
    0,
    "srlv",     // 0x06
    "srav",     // 0x07
    "jr",       // 0x08
    "jalr",     // 0x09
    0, 0,
    "syscall",  // 0x0C
    "break",    // 0x0D
    0, 0,
    "mfhi",     // 0x10
    "mthi",     // 0x11
    "mflo",     // 0x12
    "mtlo",     // 0x13
    0, 0, 0, 0,
    "mult",     // 0x18
    "multu",    // 0x19
    "div",      // 0x1A
    "divu",     // 0x1B
    0, 0, 0, 0,
    "add",      // 0x20
    "addu",     // 0x21
    "sub",      // 0x22
    "subu",     // 0x23
    "and",      // 0x24
    "or",       // 0x25
    "xor",      // 0x26
    "nor",      // 0x27
    0, 0,
    "slt",      // 0x2A
    "sltu",     // 0x2B
};

static const char * opcodeAsBranch[] = {
    "bltz",     // 0x00
    "bgez",     // 0x01
};

static const char * opcode[] = {
    0, 0,
    "j",        // 0x02
    "jal",      // 0x03
    "beq",      // 0x04
    "bne",      // 0x05
    "blez",     // 0x06
    "bgtz",     // 0x07
    "addi",     // 0x08
    "addiu",    // 0x09
    "slti",     // 0x0A
    "sltiu",    // 0x0B
    "andi",     // 0x0C
    "ori",      // 0x0D
    "xori",     // 0x0E
    "lui",      // 0x0F
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    "lb",       // 0x20
    "lh",       // 0x21
    0,
    "lw",       // 0x23
    0, 0, 0, 0,
    "sb",       // 0x28
    "sh",       // 0x29
    0,
    "sw",       // 0x2B
};

const char * mips::get_mnemonic(InstructionWord i)
{
    switch (i.R.opcode)
    {
    case 0x00:
        return opcodeAsZero[i.R.funct];
    case 0x01:
        return opcodeAsBranch[i.I.rt];
    }

    if (i.R.opcode < LEN(opcode))
        return opcode[i.R.opcode];

    return nullptr;
}

std::string mips::get_operands(InstructionWord i)
{
    char str[32];

    if (!get_mnemonic(i))
        return std::string("<unknown>");

    switch (i.R.opcode)
    {
    case 0x00:
        switch (i.R.funct)
        {
        case 0x00: // SLL
        case 0x02: // SRL
        case 0x03: // SRA
            snprintf(str, sizeof (str), "$%u, $%u, %u", i.R.rd, i.R.rt, i.R.shamt);
            break;
        // case 0x04, 0x06, 0x07: default (SLLV, SRLV, SRAV)
        case 0x11: // MTHI
        case 0x13: // MTLO
        case 0x08: // JR
            snprintf(str, sizeof (str), "$%u", i.R.rs);
            break;
        case 0x09: // JALR
            snprintf(str, sizeof (str), "$%u, $%u", i.R.rd, i.R.rs);
            break;
        case 0x0C: // SYSCALL
        case 0x0D: // BREAK
            return std::string();
        case 0x10: // MFHI
        case 0x12: // MFLO
            snprintf(str, sizeof (str), "$%u", i.R.rd);
            break;
        case 0x18: // MULT
        case 0x19: // MULTU
        case 0x1A: // DIV
        case 0x1B: // DIVU
            snprintf(str, sizeof (str), "$%u, $%u", i.R.rs, i.R.rd);
            break;
        default:
            snprintf(str, sizeof (str), "$%u, $%u, $%u", i.R.rd, i.R.rs, i.R.rt);
        }
        break;
    case 0x01: // BLTZ, BGEZ
        snprintf(str, sizeof (str), "$%u, %d", i.I.rs, (int) i.I.immediate);
        break;
    case 0x02: // J
    case 0x03: // JAL
        snprintf(str, sizeof (str), "0x%08x", (int)(i.J.immediate << 2));
        break;
    case 0x04: // BEQ
    case 0x05: // BNE
    case 0x06: // BLEZ
    case 0x07: // BGTZ
        snprintf(str, sizeof (str), "$%u, $%u, %d", i.I.rs, i.I.rt, (int16_t) i.I.immediate);
        break;
    case 0x08: // ADDI
    case 0x0A: // SLTI
        snprintf(str, sizeof (str), "$%u, $%u, %d", i.I.rt, i.I.rs, (int) i.I.immediate);
        break;
    case 0x09: // ADDIU
    case 0x0B: // SLTIU
        snprintf(str, sizeof (str), "$%u, $%u, %u", i.I.rt, i.I.rs, i.I.immediate);
        break;
    case 0x0C: // ANDI
    case 0x0D: // ORI
    case 0x0E: // XORI
        snprintf(str, sizeof (str), "$%u, $%u, %04x", i.I.rt, i.I.rs, (int) i.I.immediate);
        break;
    case 0x0F: // LUI
        snprintf(str, sizeof (str), "$%u, %04x", i.I.rt, (int) i.I.immediate);
        break;
    case 0x20: // LB
    case 0x21: // LH
    case 0x23: // LW
        snprintf(str, sizeof (str), "$%u, %d($%u)", i.I.rt, (int) i.I.immediate, i.I.rs);
        break;
    case 0x28: // SB
    case 0x29: // SH
    case 0x2B: // SW
        snprintf(str, sizeof (str), "$%u, %d($%u)", i.I.rt, (int) i.I.immediate, i.I.rs);
        break;
    }

    return std::string(str);
}
