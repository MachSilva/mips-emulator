#include "syscall.h"

#include <iostream>
#include <limits>
#include <string>
#include "mips.h"
#include "mips_exceptions.h"

using namespace std;

mips::SYSCODE mips::std_syscall_handler(MIPS * mips)
{
    char c;
    uint32_t i;

    /* Standard syscall services from MARS:
     * http://courses.missouristate.edu/kenvollmar/mars/Help/SyscallHelp.html */

    switch (mips->reg[MIPS_V0])
    {
    // print integer
    case 0x01:
        cout << mips->reg[MIPS_A0];
        break;
    // print string
    case 0x04:
        cout << mips->load_real_ptr<const char>(mips->reg[MIPS_A0]);
        break;
    // read integer
    case 0x05:
        cin >> (int32_t&) mips->reg[MIPS_V0];
        cin.ignore(1, EOF); // ignore any whitespace, like '\t'
        break;
    // read string
    case 0x08:
        // MARS reads strings in a such strange way! But compatibility is a must.
        i = mips->reg[MIPS_A1] - 1; // WHY?!
        if (0 > (int) i)
            i = 0;
        
        // Who are the guys that developed these syscalls?
        for (auto p = mips->load_real_ptr<char>(mips->reg[MIPS_A0]), e = p+i; p < e; p++)
        {
            c = cin.get();
            if (c == EOF)
                break;
            *p = c;
            if (c == '\n')
                break;
        }

        // Ignore all chars until newline
        if (c != '\n')
            cin.ignore(numeric_limits<streamsize>::max(), '\n');

        break;
    // exit
    case 0x0A:
        return SYSCODE::EXIT_REQUEST;
    // print character
    case 0x0B:
        cout << (char) mips->reg[MIPS_A0];
        break;
    // read character
    case 0x0C:
        cin >> c;
        mips->reg[MIPS_V0] = c;
        cin.ignore(1, EOF); // ignore any whitespace, like '\t'
        break;
    // print integer in hexadecimal
    case 0x22:
        printf("0x%08x", mips->reg[MIPS_A0]);
        break;
    // print integer in binary
    case 0x23:
        {
            char str[33];
            i = mips->reg[MIPS_A0];
            // "-funroll-loops" :D
            for (int k = 0; k < 32; k++)
            {
                str[k] = i & 0x80000000 ? '1' : '0';
                i <<= 1;
            }
            str[32] = 0;
            cout << str;
        }
        break;
    // print integer as unsigned
    case 0x24:
        cout << (uint32_t) mips->reg[MIPS_A0];
        break;
    }

    return SYSCODE::OK;
}
