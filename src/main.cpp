/**
 * @file    main.cpp
 * @author  Felipe Silva
 * @brief   Main file of mips-emulator
 * @version 0.1
 * @date    2018-10-09
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#include <fstream>
#include <iostream>
#include <unordered_map>
#include <ctime>

#include "mips/mips.h"
#include "mips/disassembler.h"

using namespace std;
using namespace mips;

// Commandline flags
unordered_map<string, bool> flags;

/**
 * @brief Process shell arguments and emulate
 * 
 * Initializes MIPS virtual machine and load the binaries
 * specified by shell arguments.
 * 
 * @param argc Param count
 * @param argv Shell params
 * @return int Error code
 */
int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        cerr << "Usage: " << argv[0] << " <text.bin> <data.bin> [options]\n"
             << "\t-p: print instruction being processed into stderr\n"
             << "\t-s: print statistics\n";
        return 0;
    }

    /* Parse options */
    for (int i = 3; i < argc; i++)
        flags[string(argv[i])] = true;
    
    fstream text (argv[1], ios_base::in);
    fstream data (argv[2], ios_base::in);

    // Initialize MIPS virtual machine
    MIPS vm (MIPS_DEFAULT_RAM_SIZE);
    vm.reg[MIPS_GP] = 0x1800;
    vm.reg[MIPS_SP] = 0x3FFC;
    
    // Load text and data sections
    text.read((char*) vm.ram+MIPS_TEXT, MIPS_SECT_MAX);
    data.read((char*) vm.ram+MIPS_DATA, MIPS_SECT_MAX);

    // User options
    if (flags.find("-p") != flags.end())
    {
        vm.monitor = [](MIPS * mips)
        {
            auto i = mips->load<InstructionWord>(mips->pc);
            fprintf(stderr, "%08x:   %-8s %-20s $%-2d = 0x%08x, $%-2d = 0x%08x, $%-2d = 0x%08x\n",
                mips->pc,
                get_mnemonic(i),
                get_operands(i).c_str(),
                i.R.rs, mips->reg[i.R.rs],
                i.R.rt, mips->reg[i.R.rt],
                i.R.rd, mips->reg[i.R.rd]
            );
        };
    }

    auto c = clock();
    
    try
    {
        vm.execute();
    }
    catch (mips_exception& e)
    {
        cout << e.what() << endl;
    }

    c = clock() - c;
    
    cout << '\n';
    vm.print_registers(cout);
    vm.print_memory(cout);

    if (flags.find("-s") != flags.end())
    {
        auto s = c / (double) CLOCKS_PER_SEC;
        fprintf (stderr, "%d instructions processed\n"
            "Average of %.2lfk instructions per second\n"
            "Used %.3lfs from CPU\n",
            vm.instruction_counter, (vm.instruction_counter / s) / 1000, s);
    }

    return 0;
}
