\documentclass[a4paper]{article}
\usepackage{indentfirst}
\usepackage[portuguese]{babel}
\usepackage{multicol}
\usepackage{hyperref}

\title{Relatório: projeto de Emulador MIPS}
\author{Felipe Machado da Silva}
\date{18 de Outubro, 2018}

\begin{document}
\maketitle

\section{Integrantes do grupo}

\begin{itemize}
    \item Felipe Machado da Silva; RGA: 2017.1904.014-1
\end{itemize}

\section{Funcionamento do emulador}

O emulador foi implementado numa classe chamada \emph{MIPS} que guarda todas as informações a respeito de registradores e memória RAM.
Ela contém o método \emph{execute()} que faz todo o processo de interpretação de instruções.

\label{FILES}
A seguir os detalhes serão explicados de arquivo por arquivo.

\subsection{mips.h, mips.cpp}

Contém a descrição da classe \emph{MIPS} que implementa o emulador.
Assim que construída, a memória requisitada pelo \emph{caller} é alocada e zerada, junto com todos os registradores.

O \emph{header} define o tipo \emph{InstructionWord} que é uma abstração para uma palavra de instrução do processador, que pode ser do tipo R, I ou J. Por isso, foi implementada como \emph{union} de três estruturas R, I e J. A saber, cada \emph{struct} representa um tipo, e é declarada com \emph{bit fields} para que facilite a leitura das instruções e de seus campos, sem necessidade de operações bit a bit.

\begin{verbatim}
    /* MIPS instruction types */
    union InstructionWord {
        /* Type R */
        struct __attribute__((__packed__)) {
            uint8_t funct:6, shamt:5, rd:5, rt:5, rs:5, opcode:6;
        } R;
        /* Type I */
        struct __attribute__((__packed__)) {
            int16_t immediate;
            uint8_t rt:5, rs:5, opcode:6;
        } I;
        /* Type J */
        struct __attribute__((__packed__)) {
            int32_t immediate:26;
            uint8_t opcode:6;
        } J;
    };
\end{verbatim}

Note que a ordem dos campos está invertido para estar de acordo com a configuração Little-Endian. Mais a frente existe um \verb|static_assert| para garantir que \emph{InstructionWord} realmente tenha 32 bits e não venha a causar problemas.

Há três métodos \emph{template} úteis para acesso da memória RAM da pequena máquina virtual. Todas recebem o endereço de memória em questão. Definidas \emph{inline} e no \emph{header} para serem utilizadas por qualquer trecho de código:

\begin{itemize}
    \item \verb|load_real_ptr|;
    \item \verb|load|;
    \item \verb|store|;
\end{itemize}

Por exemplo, se o registrador \texttt{\$sp} for igual a \texttt{0x3af0}, é possível carregar qualquer tipo de dado, inclusive um \emph{halfword} (16 bits), usando:

\begin{itemize}
    \item \texttt{load<int16\_t>(0x3af0);} ou
    \item \texttt{load<int16\_t>(reg[MIPS\_SP]);}
\end{itemize}

\subsection{syscall.h, syscall.cpp}

Contém o interpretador padrão de \emph{syscall} compátivel com o MARS.
Todos os serviços requisitados pelo trabalho foram implementados.
A função recebe uma instância da máquina para livre alteração e retorna um SYSCODE, que informa à rotina de executado o que deve ser feito: nada ou fechar o programa.

\subsection{disassembler.h, disassembler.cpp}

Definem duas funções que leem uma \emph{InstructionWord} e retornam o mnemônico (\texttt{get\_mnemonic}) e seus parâmetros (\texttt{get\_operands}) como \emph{string}.

Foram implementados internamente com tabelas de consulta a partir dos \emph{opcode} e \emph{funct}.

\subsection{mips\_exceptions.h}

Definem exceções que o interpretador pode disparar:

\begin{itemize}
    \item mips\_exception: classe pai;
    \item stack\_overflow: transbordo da pilha;
    \item invalid\_access: acesso não permitido à memória;
    \item divide\_by\_zero: divisão por zero.
\end{itemize}

\section{Funcionalidades implementadas}

\subsection{Instruções}

O seguinte conjunto de instruções foi implementado:

\begin{itemize}
    \item Aritmética: \texttt{SLL, SRL, SRA, SLLV, SRLV, SRAV, MULT, MULTU, DIV, DIVU, ADD, ADDU, SUB, SUBU, ADDI, ADDIU};
    \item Lógica: \texttt{AND, OR, XOR, NOR, SLT, SLTU, SLTI, SLTIU, ANDI, ORI, XORI}; 
    \item Desvio: \texttt{JR, JARL, JAL, BLTZ, BGEZ, J, JAL, BEQ, BNE, BLEZ, BGTZ};
    \item Transferência de dados: \texttt{MFHI, MTHI, MFLO, MTLO, LUI, LB, LH, LW, SB, SH, SW};
    \item Diversos: \texttt{SYSCALL, BREAK};
\end{itemize}

\subsection{Recursos}

Dois recursos extras foram incluídos. A saber:

\begin{itemize}
    \item Um \emph{disassembler} para visualização da instrução sendo executada em tempo real (ativado com a opção \texttt{-p});
    \item e uma medição do tempo gasto pelo método \emph{MIPS::execute()} usando \emph{clock\_t} (opção \texttt{-s}).
\end{itemize}

\subsection{Syscalls}

Os Syscalls foram implementados de maneira modular, ou seja, por padrão a classe inicializa o com os serviços compatíveis com o MARS.
Entretanto, o \emph{syscall handler} pode ser modificado à escolha do programador.

Os serviços disponíveis são:

\newcommand\setitemnumber[1]{\setcounter{enumi}{\numexpr#1-1\relax}}

\begin{multicols}{2}
\begin{enumerate}
    \setitemnumber{1} \item Print Integer;
    \setitemnumber{4} \item Print String;
    \setitemnumber{5} \item Read Integer;
    \setitemnumber{8} \item Read String;
    \setitemnumber{10} \item Exit;
    \setitemnumber{11} \item Print Character;
    \setitemnumber{12} \item Read Character;
    \setitemnumber{34} \item Print Integer in Hexadecimal;
    \setitemnumber{35} \item Print Integer in Binary;
    \setitemnumber{36} \item Print Integer as Unsigned.
\end{enumerate}
\end{multicols}

\subsection{Soluções de implementação}

Os templates utilitários \texttt{load\_real\_ptr, load, store} em conjunto com a definição de \emph{InstructionWord} com \emph{bit fields} permitem leituras práticas de qualquer valor, em especial da instrução a ser executada, que foi reduzida à linha:

\begin{verbatim}
    auto ins = load<InstructionWord>(pc);
\end{verbatim}

A execução do assembly MIPS é feita num laço \emph{while} que busca palavras e utiliza um \emph{switch} para decodificar a instrução.
Assim como a função membro \emph{syscall}, a função \emph{monitor} pode ser escolhida à vontade pelo desenvolvedor para ser chamada antes de cada instrução ser executada.

\subsection{Organização do código fonte}

\label{TESTING}
Os arquivos teste em assembly estão sob a pasta \texttt{/data}.
Cada teste contém o código fonte \texttt{.asm} e um arquivo de entrada \texttt{.in}.
O \emph{makefile} é capaz de montá-los e executá-los para gerar um arquivo de saída \texttt{.sol}, com o auxílio do MARS.

O código fonte do emulador está sob a pasta \texttt{/src}.
Nela temos, \texttt{main.cpp} com o código principal e a pasta \texttt{mips} com a declaração e definição da classe responsável pela emulação (ver \ref{FILES}).

\section{Qualidade do código}

O código está dividido em vários arquivos.
Todos estão documentados de maneira compatível com a ferramenta Doxygen.

\section{Funcionalidades não implementadas}

A ideia original e escalável seria implementar o conjunto de instruções usando classes com nome, métodos para executar e desmontar a instrução.
Porém, para evitar a sobrecarga de calculo de endereço e chamada de função, o código de simulação foi escrita diretamente num \emph{switch-case}.
Entretanto, existem métodos melhores para desempenho como AOT e JIT, ao invés de interpretação.

\subsection{Dificuldades encontradas}

A principal dificuldade foi testar o código e depurar em busca de instruções mal implementadas.
Apesar do MIPS ser um processador RISC, testar instrução por instrução é humanamente cansativo.

\section{Casos de teste}

Todos os casos de teste se encontram sob a pasta \texttt{/data}.
O \emph{Makefile} contém regras para montar os testes e gerar arquivos de saída (ver \ref{TESTING});

\end{document}
