# Program for SYSCALL testing

.data
title:      .asciiz "SYSCALL TEST\n"
msg:        .ascii  "Hi, there! This is my little message:\n"
            .asciiz "Seek knowledge!\n"

input:      .asciiz "Your input: "
buffer:     .byte 0 : 128

.text
main:
    # Title :D
    li      $v0, 4
    la      $a0, title
    syscall

    # 0x01: print integer
    li      $v0, 1
    li      $a0, 256        # integer to print
    syscall
    jal     print_newline

    # 0x04: print string
    li      $v0, 4
    la      $a0, msg
    syscall

    # 0x05: read integer
    li      $v0, 5
    syscall
    move    $a0, $v0
    li      $v0, 1          # print integer we read
    syscall
    jal     print_newline   # newline

    # 0x08: read string
    li      $v0, 8
    la      $a0, buffer
    la      $a1, 128        # max length
    syscall
    li      $v0, 4
    la      $a0, input      # print string we read
    syscall
    li      $v0, 4
    la      $a0, buffer
    syscall
    jal     print_newline

    # 0x0B: print character
    li      $v0, 11
    li      $a0, '$'
    syscall
    jal     print_newline

    # 0x0C: read character
    li      $v0, 12
    syscall
    # print character we read
    move    $a0, $v0
    li      $v0, 11
    syscall
    jal     print_newline

    # 0x22: print integer in hexadecimal
    li      $v0, 34
    move    $a0, $sp
    syscall
    jal     print_newline

    # 0x23: print integer in binary
    li      $v0, 35
    move    $a0, $sp
    syscall
    jal     print_newline

    # 0x24: print integer as unsigned
    li      $v0, 36
    la      $a0, -16384
    syscall
    jal     print_newline

    # Exit
    li		$v0, 10
    syscall

.data
newline:    .asciiz "\n"

.text
print_newline:
    li      $v0, 4
    la      $a0, newline
    syscall
    jr      $ra
