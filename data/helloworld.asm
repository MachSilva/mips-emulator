# Simple Hello World!

.data
msg:        .asciiz "Hi! :D"

.text
    la      $a0, msg
    li      $v0, 4      # print string syscall code
    nop
    syscall

    li      $v0, 10     # exit syscall
    syscall             # good bye!
    