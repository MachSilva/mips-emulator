# Integer Average from numbers

.data
v:          .space 128
v_size:     .word 0

msg0:       .asciiz "MIPS AVG TEST\n"
msg1:       .asciiz "Average = "
msg2:       .asciiz "Remainder: "
snewline:   .asciiz "\n"

.text
main:
    # Header
    li      $v0, 4          # print string syscall
    la      $a0, msg0
    syscall

    # Read count
    li      $v0, 5          # read integer syscall
    syscall
    la      $s0, v_size
    sw      $v0, 0($s0)     # store count in v_size
    move    $s0, $v0

    # Read all numbers and accumulate
    # $s0: v_size
    # $s1: acc
    # while (v_size-- > 0)
    
    xor     $s1, $s1, $s1   # acc = 0

L00:
    blez    $s0, L00e

    # Read number
    li      $v0, 5          # read integer syscall
    syscall
    add     $s1, $s1, $v0   # acc += k

    sub     $s0, $s0, 1
    j       L00

L00e:

    # Compute result
    la      $t0, v_size
    lw      $s0, 0($t0)
    div     $s1, $s0        # acc / v_size
    mflo    $s0             # $s0 = floor(acc / v_size)
    mfhi    $s1             # $s1 = acc % v_size

    # Print results
    # Average
    li      $v0, 4          # print string
    la      $a0, msg1
    syscall
    
    li      $v0, 1          # print integer
    move    $a0, $s0
    syscall
    
    jal     print_newline
    
    # Remainder
    # Average
    li      $v0, 4          # print string
    la      $a0, msg2
    syscall
    
    li      $v0, 1          # print integer
    move    $a0, $s1
    syscall
    
    jal     print_newline

    # Exit
    li      $v0, 10
    syscall

print_newline:
    li      $v0, 4
    la      $a0, snewline
    syscall

    jr      $ra
