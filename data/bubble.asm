# Bubble sort in MIPS Assembly

.data
title:      .asciiz "BUBBLE SORT!\n"
array_size: .word 0
array:      .space 512      # 128 integers

.text
main:
    # Print Title
    li      $v0, 4          # print string syscall
    la      $a0, title
    syscall

    # Read array size
    li      $v0, 5          # read integer syscall
    syscall
    la      $t0, array_size
    sw      $v0, 0($t0)     # array_size = $v0
    move    $s0, $v0        # $s0: array_size
    
    # for (int i = 0; i < array_size; i++)
    # -> for (int* p = array; p < array+array_size; p++)
    sll     $s0, $v0, 2     # $s0 *= 4
    la      $s1, array      # $s1: p = array
    add     $s0, $s0, $s1   # $s0 = array+array_size

    move    $s2, $s1        # save $s1: array

L00:
    bge		$s1, $s0, L00e	# if $s1 >= $s0 then L00e
    
    # Read element
    li      $v0, 5          # read integer syscall
    syscall
    # Store in array
    sw      $v0, 0($s1)     # *p = syscall(5)

    addi    $s1, $s1, 4     # p++
    j       L00

L00e:
    # Bubble sort!
    la      $a0, array
    la      $t0, array_size
    lw      $a1, 0($t0)
    jal     bubble_sort

    # Print array
    # $s2: array, saved earlier
    # $s0: array + array_size
L01:
    bge     $s2, $s1, L01e

    # print element
    li      $v0, 1          # print integer syscall
    lw      $a0, 0($s2)
    syscall

    # print separator
    li      $v0, 11         # print character syscall
    li      $a0, ' '
    syscall

    addi    $s2, $s2, 4     # p++
    j       L01

L01e:
    # Exit
    li      $v0, 10
    syscall
    
    # :D
    break
    nop

bubble_sort:
    sll     $t0, $a1, 2
    move    $t1, $a0        # $t1: i = v
    add     $t0, $t0, $a0   # $t0: j = v+n

L03:
    bge     $t1, $t0, L03e  # while i < v+n
    move    $t2, $t1        # $t2: j = i

L04:
    bge     $t2, $t0, L04e  # while j < v+n
    
    lw      $t3, 0($t1)     # $t3: *i
    lw      $t4, 0($t2)     # $t4: *j
    ble     $t3, $t4, L05   # if (*i > *j) then
    sw      $t3, 0($t2)     #       swap(i, j)
    sw      $t4, 0($t1)

L05:
    addi    $t2, $t2, 4
    j       L04

L04e:
    addi    $t1, $t1, 4
    j       L03

L03e:
    jr      $ra

# ALGO
# void bubble_sort(int v[], int n)
# {
#     for (int i = 0; i < n; i++)
#         for (int j = i; j < n; j++)
#             if (v[i] > v[j])
#                 swap(v+i, v+j);
# }
