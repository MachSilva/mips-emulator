# Sum evaluation vulnerable to stack overflow

.data
msg:        .ascii  "Recursive SUM evaluation\n"
            .ascii  "Just send me a big number!\n"
            .asciiz "SUM from 1 to ?\n"
newln:      .asciiz "\n"

.text
main:
    # show message
    li      $v0, 4              # print string syscall
    la      $a0, msg
    syscall

    # read number
    li      $v0, 5              # read integer syscall
    syscall

    # SUM!
    move    $a0, $v0
    jal     sum_until_zero
    move    $s0, $v0            # save result

    # newline
    li      $v0, 4
    la      $a0, newln
    syscall

    # print result
    li      $v0, 1              # print integer
    move    $a0, $s0
    syscall

    # newline
    li      $v0, 4
    la      $a0, newln
    syscall

    # exit
    li      $v0, 10
    syscall
    break
    nop     # :D

# int sum_until_zero(int n)
sum_until_zero:
    # if (n < 0) return 0;
    xor     $v0, $v0, $v0
    ble     $a0, $zero, L00

    sw      $a0, -4($sp)
    sw      $ra, -8($sp)
    sub     $sp, $sp, 8         # save $ra, $a0 on stack

    sub     $a0, $a0, 1
    jal     sum_until_zero

    lw      $ra, 0($sp)
    lw      $a0, 4($sp)
    add     $sp, $sp, 8         # restore
    
    add     $v0, $v0, $a0       # sum and return

L00:
    jr      $ra

